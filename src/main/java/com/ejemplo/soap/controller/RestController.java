package com.ejemplo.soap.controller;

import com.ejemplo.soap.client.gen.EmpleadoRS;
import com.ejemplo.soap.exception.CustomException;
import com.ejemplo.soap.service.SoapService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rest")
@CrossOrigin("*")
@AllArgsConstructor
public class RestController {

    private SoapService soapService;

    @GetMapping("/save")
    public ResponseEntity<? extends Object> test(@RequestParam("nombres") final String nombre, @RequestParam("apellidos") final String apellidos, @RequestParam("tipo_documento") final String tipo_documento, @RequestParam("numero_documento") final String numero_documento, @RequestParam("fecha_nacimiento") final String fecha_nacimiento, @RequestParam("fecha_vinculacion") final String fecha_vinculacion, @RequestParam("cargo") final String cargo, @RequestParam("salario") final String salario) {

        try {
            final EmpleadoRS empleadoRS = soapService.save(nombre, apellidos, tipo_documento, numero_documento, fecha_nacimiento, fecha_vinculacion, cargo, salario);
            return ResponseEntity.status(HttpStatus.OK).body(empleadoRS);
        } catch (CustomException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getRs());
        }
    }
}
