package com.ejemplo.soap.util;

import com.ejemplo.soap.client.gen.EmpleadoRQ;
import com.ejemplo.soap.client.gen.EmpleadoRS;
import com.ejemplo.soap.exception.CustomException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;

public class Util {

    private Util() {
        //Default private constructor
    }

    public static Period getEdad(final String fecha_nacimiento) {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        final LocalDate fechaNacimiento = LocalDate.parse(fecha_nacimiento, formatter);
        return Period.between(fechaNacimiento, LocalDate.now());
    }

    public static String getEdadFormato(final String fecha_nacimiento) {
        final Period period = getEdad(fecha_nacimiento);

        return new StringBuilder().append(period.getYears() > 0 ? period.getYears() + " años, " : "").append(period.getMonths() > 0 ? period.getMonths() + " meses y " : "").append(period.getDays()).append(" dias").toString();
    }

    public static String getTiempoTranscurrido(final String dt) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fecha_calculo = LocalDate.parse(dt, formatter);

        final Period period = Period.between(fecha_calculo, LocalDate.now());
        return new StringBuilder().append(period.getYears() > 0 ? period.getYears() + " años, " : "").append(period.getMonths() > 0 ? period.getMonths() + " meses y " : "").append(period.getDays()).append(" dias").toString();
    }

    public static void isMayor(final String fecha_nacimiento) throws CustomException {
        final boolean isMayor = (getEdad(fecha_nacimiento).getYears() >= 18);
        if (!isMayor) {
            throw new CustomException("OK", "El empleado es menor de edad");
        }
    }

    public static void nonEmptyFields(final EmpleadoRQ emp) throws CustomException {
        final boolean nonEmpty = !emp.getNombres().isEmpty() && !emp.getApellidos().isEmpty() && !emp.getTipoDocumento().isEmpty() && !emp.getNumeroDocumento().isEmpty() && !emp.getFechaNacimiento().isEmpty() && !emp.getFechaVinculacion().isEmpty() && !emp.getCargo().isEmpty() && !emp.getSalario().isEmpty();

        if (!nonEmpty) throw new CustomException("OK", "Error en la validación de campos");
    }

    public static void validateDateFormat(final String date) throws CustomException {
        final DateFormat sdf = new SimpleDateFormat("dd/MM/yyyyy");
        sdf.setLenient(false);
        try {
            sdf.parse(date);
        } catch (ParseException e) {
            throw new CustomException("OK", "La fecha " + date + " es invalida");
        }
    }

    public static EmpleadoRQ createRQ(final String nombres, final String apellidos, final String tipo_documento, final String numero_documento, final String fecha_nacimiento, final String fecha_vinculacion, final String cargo, final String salario) {
        final EmpleadoRQ rq = new EmpleadoRQ();
        rq.setNombres(nombres);
        rq.setApellidos(apellidos);
        rq.setTipoDocumento(tipo_documento);
        rq.setNumeroDocumento(numero_documento);
        rq.setFechaNacimiento(fecha_nacimiento);
        rq.setFechaVinculacion(fecha_vinculacion);
        rq.setCargo(cargo);
        rq.setSalario(salario);
        return rq;
    }

    public static EmpleadoRS createRS(final EmpleadoRQ rq, final String tiempoVinculacion, final String edad) {
        final EmpleadoRS rs = new EmpleadoRS();
        rs.setNombres(rq.getNombres());
        rs.setApellidos(rq.getApellidos());
        rs.setTipoDocumento(rq.getTipoDocumento());
        rs.setNumeroDocumento(rq.getNumeroDocumento());
        rs.setFechaNacimiento(rq.getFechaNacimiento());
        rs.setFechaVinculacion(rq.getFechaVinculacion());
        rs.setCargo(rq.getCargo());
        rs.setSalario(rq.getSalario());
        rs.setTiempoVinculacion(tiempoVinculacion);
        rs.setEdad(edad);
        return rs;
    }
}
