package com.ejemplo.soap;

import com.ejemplo.soap.client.gen.SaveEmployeeRQ;
import com.ejemplo.soap.client.gen.SaveEmployeeRS;
import com.ejemplo.soap.service.EmpleadoService;
import com.ejemplo.soap.exception.CustomException;
import lombok.AllArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@AllArgsConstructor
public class EmpleadoEndpoint {

    private static final String NAMESPACE_URI = "rest-soap-namespace";

    private EmpleadoService empleadoService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveEmployeeRQ")
    @ResponsePayload
    public SaveEmployeeRS save(@RequestPayload SaveEmployeeRQ rq) throws CustomException {
        return empleadoService.save(rq);
    }
}
