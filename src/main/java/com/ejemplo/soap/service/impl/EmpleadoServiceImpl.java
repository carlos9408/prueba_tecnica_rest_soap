package com.ejemplo.soap.service.impl;

import com.ejemplo.soap.client.gen.EmpleadoRQ;
import com.ejemplo.soap.client.gen.EmpleadoRS;
import com.ejemplo.soap.client.gen.SaveEmployeeRQ;
import com.ejemplo.soap.client.gen.SaveEmployeeRS;
import com.ejemplo.soap.model.Empleado;
import com.ejemplo.soap.repository.EmpleadoRepository;
import com.ejemplo.soap.service.EmpleadoService;
import com.ejemplo.soap.exception.CustomException;
import com.ejemplo.soap.util.Util;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@AllArgsConstructor
public class EmpleadoServiceImpl implements EmpleadoService {

    private static final Logger logger = LoggerFactory.getLogger(EmpleadoServiceImpl.class);

    private EmpleadoRepository empleadoRepository;

    @Override
    public SaveEmployeeRS save(SaveEmployeeRQ rq) throws CustomException {
        try {
            final EmpleadoRQ empleadoRQ = rq.getEmpleadoRQ();

            final Date fecha_nacimiento = (new SimpleDateFormat("dd/MM/yyyy")).parse(empleadoRQ.getFechaNacimiento());
            final Date fecha_vinculacion = (new SimpleDateFormat("dd/MM/yyyy")).parse(empleadoRQ.getFechaVinculacion());
            final Double salario = Double.parseDouble(empleadoRQ.getSalario());

            final Empleado emp = Empleado.builder().
                    nombres(empleadoRQ.getNombres()).
                    apellidos(empleadoRQ.getApellidos()).
                    tipoDocumento(empleadoRQ.getTipoDocumento()).
                    numero_documento(empleadoRQ.getNumeroDocumento()).
                    fechaNacimiento(fecha_nacimiento).
                    fechaVinculacion(fecha_vinculacion).
                    cargo(empleadoRQ.getCargo()).
                    salario(salario).
                    build();

            logger.info("before save empleado");
            empleadoRepository.save(emp);
            logger.info("after save empleado");

            final String edad = Util.getEdadFormato(empleadoRQ.getFechaNacimiento());
            final String tiempoVinculacion = Util.getTiempoTranscurrido(empleadoRQ.getFechaVinculacion());

            final EmpleadoRS rs = Util.createRS(empleadoRQ, tiempoVinculacion, edad);
            final SaveEmployeeRS finalRS = new SaveEmployeeRS();
            finalRS.setEmpleadoRS(rs);
            return finalRS;
        } catch (ParseException e) {
            throw new CustomException("ERROR", "Error interno del sistema, por favor comuniquese con el administrador");
        }


    }

}
