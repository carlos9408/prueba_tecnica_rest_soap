package com.ejemplo.soap.service.impl;

import com.ejemplo.soap.client.EmpleadoClient;
import com.ejemplo.soap.client.gen.EmpleadoRQ;
import com.ejemplo.soap.client.gen.EmpleadoRS;
import com.ejemplo.soap.controller.RestController;
import com.ejemplo.soap.service.SoapService;
import com.ejemplo.soap.exception.CustomException;
import com.ejemplo.soap.util.Util;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class SoapServiceImpl implements SoapService {

    private static final Logger logger = LoggerFactory.getLogger(RestController.class);

    private EmpleadoClient client;

    @Override
    public EmpleadoRS save(String nombres, String apellidos, String tipo_documento, String numero_documento,
                           String fecha_nacimiento, String fecha_vinculacion, String cargo, String salario) throws CustomException {

        final EmpleadoRQ rq = Util.createRQ(nombres, apellidos, tipo_documento, numero_documento, fecha_nacimiento, fecha_vinculacion, cargo, salario);

        logger.info("Valida campos vacios");
        Util.nonEmptyFields(rq);
        logger.info("Valida fechas");
        Util.validateDateFormat(fecha_nacimiento);
        Util.validateDateFormat(fecha_vinculacion);
        logger.info("Valida mayor de edad");
        Util.isMayor(fecha_nacimiento);
        return client.save(rq);
    }
}
