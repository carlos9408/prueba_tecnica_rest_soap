package com.ejemplo.soap.service;

import com.ejemplo.soap.client.gen.EmpleadoRS;
import com.ejemplo.soap.exception.CustomException;
import org.springframework.stereotype.Service;

@Service
public interface SoapService {

    EmpleadoRS save(String nombres, String apellidos, String tipo_documento, String numero_documento,
                    String fecha_nacimiento, String fecha_vinculacion, String cargo, String salario) throws CustomException;
}
