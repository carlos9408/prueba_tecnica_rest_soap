package com.ejemplo.soap.service;

import com.ejemplo.soap.client.gen.SaveEmployeeRQ;
import com.ejemplo.soap.client.gen.SaveEmployeeRS;
import com.ejemplo.soap.exception.CustomException;
import org.springframework.stereotype.Service;

@Service
public interface EmpleadoService {

    SaveEmployeeRS save(SaveEmployeeRQ rq) throws CustomException;
}
