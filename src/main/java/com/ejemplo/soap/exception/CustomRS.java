package com.ejemplo.soap.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CustomRS {

    private static final long serialVersionUID = 7718828512143293558L;

    private final String timestamp;
    private final String status;
    private final String error;
    private final String message;
}
