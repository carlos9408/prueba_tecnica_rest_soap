package com.ejemplo.soap.exception;

import lombok.Getter;

import java.time.LocalDate;

@Getter
public class CustomException extends Exception {

    private static final long serialVersionUID = 7718828512143293558L;

    private final String code;
    private final String msg;
    private CustomRS rs;

    public CustomException(final String code, final String msg) {
        super();
        this.code = code;
        this.msg = msg;
        this.rs = new CustomRS(LocalDate.now().toString(), code, "Internal Server Error", msg);
    }
}
