
package com.ejemplo.soap.client.gen;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="empleadoRS" type="{rest-soap-namespace}empleadoRS"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empleadoRS"
})
@XmlRootElement(name = "saveEmployeeRS")
public class SaveEmployeeRS {

    @XmlElement(required = true)
    protected EmpleadoRS empleadoRS;

    /**
     * Obtiene el valor de la propiedad empleadoRS.
     * 
     * @return
     *     possible object is
     *     {@link EmpleadoRS }
     *     
     */
    public EmpleadoRS getEmpleadoRS() {
        return empleadoRS;
    }

    /**
     * Define el valor de la propiedad empleadoRS.
     * 
     * @param value
     *     allowed object is
     *     {@link EmpleadoRS }
     *     
     */
    public void setEmpleadoRS(EmpleadoRS value) {
        this.empleadoRS = value;
    }

}
