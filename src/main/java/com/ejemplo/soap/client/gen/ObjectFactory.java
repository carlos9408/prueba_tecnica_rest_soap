
package com.ejemplo.soap.client.gen;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.ejemplo.soap.client.gen package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.ejemplo.soap.client.gen
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SaveEmployeeRQ }
     * 
     */
    public SaveEmployeeRQ createSaveEmployeeRQ() {
        return new SaveEmployeeRQ();
    }

    /**
     * Create an instance of {@link EmpleadoRQ }
     * 
     */
    public EmpleadoRQ createEmpleadoRQ() {
        return new EmpleadoRQ();
    }

    /**
     * Create an instance of {@link SaveEmployeeRS }
     * 
     */
    public SaveEmployeeRS createSaveEmployeeRS() {
        return new SaveEmployeeRS();
    }

    /**
     * Create an instance of {@link EmpleadoRS }
     * 
     */
    public EmpleadoRS createEmpleadoRS() {
        return new EmpleadoRS();
    }

}
