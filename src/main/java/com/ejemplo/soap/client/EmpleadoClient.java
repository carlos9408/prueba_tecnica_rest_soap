package com.ejemplo.soap.client;

import com.ejemplo.soap.client.gen.EmpleadoRQ;
import com.ejemplo.soap.client.gen.EmpleadoRS;
import com.ejemplo.soap.client.gen.SaveEmployeeRQ;
import com.ejemplo.soap.client.gen.SaveEmployeeRS;
import com.ejemplo.soap.exception.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class EmpleadoClient extends WebServiceGatewaySupport {

    private static final Logger logger = LoggerFactory.getLogger(EmpleadoClient.class);

    public EmpleadoRS save(EmpleadoRQ rq) throws CustomException {
        try {
            final SaveEmployeeRQ request = new SaveEmployeeRQ();
            request.setEmpleadoRQ(rq);
            logger.info("Requesting information for " + rq);
            logger.info("before soap");
            final SaveEmployeeRS response = (SaveEmployeeRS) getWebServiceTemplate().marshalSendAndReceive(request);
            logger.info("post soap");
            return response.getEmpleadoRS();
        } catch (Exception e) {
            throw new CustomException("ERROR", "Error interno del sistema, por favor comunicarse con el administrador");
        }
    }
}