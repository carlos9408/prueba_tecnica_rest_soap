package com.ejemplo.soap.client;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class EmpleadoClientConfig {

    @Bean
    public Jaxb2Marshaller marshaller() {
            Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
            marshaller.setContextPath("com.ejemplo.soap.client.gen");
            return marshaller;
    }

    @Bean
    public EmpleadoClient employeeClient(Jaxb2Marshaller marshaller) {
            EmpleadoClient client = new EmpleadoClient();
            client.setDefaultUri("http://localhost:8001/soap");
            client.setMarshaller(marshaller);
            client.setUnmarshaller(marshaller);
            return client;
    }
}